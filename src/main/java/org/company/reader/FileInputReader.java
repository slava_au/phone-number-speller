package org.company.reader;

import org.company.helper.StringUtils;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

/**
 * Filesystem reader.
 *
 * @author Slava Ustovytski
 */
public class FileInputReader implements InputReader {

    private final List<String> filenames;

    public FileInputReader(List<String> filenames) {
        this.filenames = filenames;
    }

    /**
     * Reads data from file on the local filesystem.
     *
     * @return returns a list of line read from the file.
     */
    @Override
    public List<String> readLines() {
        List<String> fileInput = new ArrayList<String>();
        System.out.println("\nUsing file as an input: ");
        for (String filename : filenames) {
            System.out.println(filename);
            Scanner scanner = null;
            try {
                scanner = new Scanner(new FileInputStream(filename));
                while (scanner.hasNext()) {
                    String line = scanner.nextLine();
                    if (StringUtils.isNotEmpty(line)) {
                        fileInput.add(line);
                    }
                }
            } catch (IOException e) {
                System.err.printf("ERROR: Could not read from the file (%s). Details: %s. %n", filename, e.getMessage());
            } finally {
                if (scanner != null) {
                    scanner.close();
                }
            }
        }

        return fileInput;
    }
}
