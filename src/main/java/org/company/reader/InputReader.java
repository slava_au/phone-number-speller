package org.company.reader;

import java.util.List;

/**
 * Basic interface of the input reader. Can be a ConsoleReader, FileReader, etc.
 */
public interface InputReader {

    /**
     * Reads data from known source and return every line as a list item.
     *
     * @return a list containing read line from the source.
     */
    List<String> readLines();

}
