package org.company.reader;

import org.company.helper.StringUtils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 * Reads input from console.
 *
 * @author Slava Ustovytski
 */
public class ConsoleInputReader implements InputReader {

    private BufferedReader consoleReader;

    public ConsoleInputReader() {
        this(new BufferedReader(new InputStreamReader(System.in)));
    }

    public ConsoleInputReader(BufferedReader consoleReader) {
        this.consoleReader = consoleReader;
    }

    /**
     * Reads inputs from console. New empty line is an indicator of finished input.
     *
     * @return
     */
    @Override
    public List<String> readLines() {
        List<String> userInput = new ArrayList<String>();

        System.out.println("\nEnter phone numbers here (use new empty line to finish your input):");
        String line;
        try {
            while((line = consoleReader.readLine()) != null && StringUtils.isNotEmpty(line)) {
                    userInput.add(line);
            }
        } catch (Exception e) {
            System.err.println("Error occurred while reading input from the console. Details: " + e.getMessage());
            System.exit(1);
        } finally {
            if(consoleReader != null) {
                try {
                    consoleReader.close();
                } catch (IOException e) {
                    System.err.println("Error occurred while closing consoleReader. Details: " + e.getMessage());
                }
            }
        }
        return userInput;
    }
}
