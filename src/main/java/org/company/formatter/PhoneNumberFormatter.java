package org.company.formatter;

import java.util.List;

/**
 * Formatter interface.
 *
 * @author Slava Ustovytski
 */
public interface PhoneNumberFormatter {

    /**
     * Formats provided list of string into a single string.
     *
     * @param phoneNumberParts list of string parts.
     *
     * @return a single string.
     */
    String format(List<String> phoneNumberParts);

}
