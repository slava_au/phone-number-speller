package org.company.formatter;

import org.company.helper.ValidationUtils;

import java.util.List;

/**
 * A default implementation of the PhoneNumberFormatter. Converts all the string to uppercase and
 * uses dashes between words.
 *
 * For example:
 * [call,9,me] -> CALL-9-ME
 *
 * @author Slava Ustovytski
 */
public class PhoneNumberFormatterImpl implements PhoneNumberFormatter {

    /**
     * Formats provided list of string.
     *
     * @param phoneNumberParts list of string parts.
     *
     * @return returns a single string.
     */
    @Override
    public String format(List<String> phoneNumberParts) {
        ValidationUtils.assertNotNull(phoneNumberParts);

        StringBuilder sb = new StringBuilder();
        for (int i=0; i<phoneNumberParts.size(); i++) {
            if(i>0) {
                sb.append("-");
            }
            sb.append(phoneNumberParts.get(i).toUpperCase());
        }
        return sb.toString();
    }
}
