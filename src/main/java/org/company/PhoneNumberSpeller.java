package org.company;

import org.company.dictionary.PhoneNumberDictionary;
import org.company.dictionary.PhoneNumberDictionaryImpl;
import org.company.formatter.PhoneNumberFormatter;
import org.company.formatter.PhoneNumberFormatterImpl;
import org.company.helper.StringUtils;
import org.company.parser.*;
import org.company.reader.ConsoleInputReader;
import org.company.reader.FileInputReader;
import org.company.reader.InputReader;

import java.util.*;

/**
 * Main entry point of the program.
 *
 * This class gets a dictionary and phone numbers as an input from user and produces output containing all the possible
 * word replacements for each number.
 *
 * Command line arguments supported by this program are:
 *
 * -d path/to/dictionary/file
 * path/to/user/input/file
 *
 * Usage:
 *
 * <program_name> [-d path/to/dictionary/file] [path/to/user/input/file]
 */
public class PhoneNumberSpeller {

    private InputReader phoneNumbersInputReader;
    private PhoneNumberParser phoneNumberParser;
    private PhoneNumberFormatter outputFormatter;

    /**
     * Reads phone numbers from specified input source and produces a formatted output of all the possible phone
     * number replacements for each provided number.
     *
     * @return a map.
     */
    public Map<String, List<String>> spell() {
        List<String> inputNumbers = phoneNumbersInputReader.readLines();
        Map<String, List<String>> outputNumbers = new HashMap<String, List<String>>();

        // parse phone numbers
        Map<String, List<ParsedPhoneNumber>> parsedData = phoneNumberParser.parse(inputNumbers);

        for(Map.Entry<String, List<ParsedPhoneNumber>> entry : parsedData.entrySet()) {
            List<String> formattedNumbers = new ArrayList<String>();
            for(ParsedPhoneNumber parsedPhoneNumber : entry.getValue()) {
                // format parsed numbers
                formattedNumbers.add(parsedPhoneNumber.getFormattedNumber(outputFormatter));
            }
            outputNumbers.put(entry.getKey(), formattedNumbers);
        }
        return outputNumbers;
    }

    public static void main(String[] args) {
        // Parse command line arguments first
        CommandLineParser cliParser = new CommandLineParser();
        CommandLineOptions arguments = new CommandLineOptions();
        arguments.addOption("-d", false);
        cliParser.parse(args, arguments);

        // construct a dictionary using a local file input.
        String dictionaryFileName = getDictionaryFileName(arguments, "./input/sample-dictionary.txt");
        InputReader dictionaryFileInputReader = new FileInputReader(Arrays.asList(dictionaryFileName));
        PhoneNumberDictionary dictionary = new PhoneNumberDictionaryImpl(dictionaryFileInputReader);
        // build phoneNumber parser which uses provided dictionary
        PhoneNumberParser phoneNumberParser = new PhoneNumberParserImpl(dictionary);
        // build phone numbers input
        InputReader phoneNumbersInputReader = createPhoneNumbersInputReader(arguments);
        PhoneNumberFormatter outputFormatter = new PhoneNumberFormatterImpl();

        PhoneNumberSpeller speller = new PhoneNumberSpeller();
        speller.setPhoneNumbersInputReader(phoneNumbersInputReader);
        speller.setPhoneNumberParser(phoneNumberParser);
        speller.setOutputFormatter(outputFormatter);
        // spell all the provided phone numbers
        Map<String, List<String>> outputNumbers = speller.spell();

        // printing the result to the standard output.
        for (Map.Entry<String, List<String>> resultNumber : outputNumbers.entrySet()) {
            System.out.printf("Possible matches for phone number (%s) are:\n", resultNumber.getKey());
            for(String formattedPhoneNumber : resultNumber.getValue()) {
                System.out.println(formattedPhoneNumber);
            }
        }
    }

    private static String getDictionaryFileName(CommandLineOptions arguments, String defaultValue) {
        String filename = defaultValue;
        // if specific dictionary file was not provided then we use default one.
        CommandLineOptions.Option option = arguments.getOptionByName("-d");
        if(option != null && StringUtils.isNotEmpty(option.getValue())) {
            filename = option.getValue();
        }
        return filename;
    }

    private static InputReader createPhoneNumbersInputReader(CommandLineOptions arguments) {
        InputReader inputReader;
        List<String> params = arguments.getParams();
        if(params != null && !params.isEmpty()) {
            // use file as an input
            inputReader = new FileInputReader(params);
        } else {
            // use console as an input
            inputReader = new ConsoleInputReader();
        }
        return inputReader;
    }

    public void setPhoneNumbersInputReader(InputReader phoneNumbersInputReader) {
        this.phoneNumbersInputReader = phoneNumbersInputReader;
    }

    public void setPhoneNumberParser(PhoneNumberParser phoneNumberParser) {
        this.phoneNumberParser = phoneNumberParser;
    }

    public void setOutputFormatter(PhoneNumberFormatter outputFormatter) {
        this.outputFormatter = outputFormatter;
    }
}
