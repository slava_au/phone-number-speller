package org.company.dictionary;

import java.util.List;

/**
 * Dictionary interface responsible for looking up words based on provided keys.
 *
 * @author Slava Ustovytski
 */
public interface PhoneNumberDictionary {

    /**
     * Finds a match for provided key.
     *
     * @param key the key
     * @return Returns a list of possible matches. Returns empty list if no matches were found.
     */
    List<String> findMatch(String key);
}
