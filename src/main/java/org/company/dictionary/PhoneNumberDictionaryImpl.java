package org.company.dictionary;


import org.company.helper.StringUtils;
import org.company.helper.ValidationUtils;
import org.company.reader.InputReader;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default PhoneNumberDictionary implementation. Uses a hashmap of reverse mappings as a dictionary back-end.
 *
 * for example for input words:
 * call, ball, me
 *
 * the dictionary structure will be:
 * 2255 > [ball, call]
 * 63 > [me]
 *
 * @author Slava Ustovytski
 */
public class PhoneNumberDictionaryImpl implements PhoneNumberDictionary {

    private static Map<Character, Integer> keypadMappings = new HashMap<Character, Integer>();

    /**
     * Mapping is using to convert dictionary words to keypad numbers.
     */
    static  {
        keypadMappings.put(Character.valueOf('a'), 2);
        keypadMappings.put(Character.valueOf('b'), 2);
        keypadMappings.put(Character.valueOf('c'), 2);

        keypadMappings.put(Character.valueOf('d'), 3);
        keypadMappings.put(Character.valueOf('e'), 3);
        keypadMappings.put(Character.valueOf('f'), 3);

        keypadMappings.put(Character.valueOf('g'), 4);
        keypadMappings.put(Character.valueOf('h'), 4);
        keypadMappings.put(Character.valueOf('i'), 4);

        keypadMappings.put(Character.valueOf('j'), 5);
        keypadMappings.put(Character.valueOf('k'), 5);
        keypadMappings.put(Character.valueOf('l'), 5);

        keypadMappings.put(Character.valueOf('m'), 6);
        keypadMappings.put(Character.valueOf('n'), 6);
        keypadMappings.put(Character.valueOf('o'), 6);

        keypadMappings.put(Character.valueOf('p'), 7);
        keypadMappings.put(Character.valueOf('q'), 7);
        keypadMappings.put(Character.valueOf('r'), 7);
        keypadMappings.put(Character.valueOf('s'), 7);

        keypadMappings.put(Character.valueOf('t'), 8);
        keypadMappings.put(Character.valueOf('u'), 8);
        keypadMappings.put(Character.valueOf('v'), 8);

        keypadMappings.put(Character.valueOf('w'), 9);
        keypadMappings.put(Character.valueOf('x'), 9);
        keypadMappings.put(Character.valueOf('y'), 9);
        keypadMappings.put(Character.valueOf('z'), 9);
    }

    private final InputReader inputReader;

    private final Map<String, List<String>> dictionary = new HashMap<String, List<String>>();

    public PhoneNumberDictionaryImpl(InputReader inputReader) {
        this.inputReader = inputReader;

        // read dictionary
        init();
    }

    private void init() {
        System.out.println("Loading dictionary...");
        List<String> content = inputReader.readLines();
        if(content == null || content.isEmpty()) {
            System.err.println("Could not read or empty dictionary file. Aborting...");
            System.exit(1);
        }

        // parsing dictionary content
        for(String line : content) {
            // remove all the non character symbols
            String currentWord = StringUtils.stripWhitespacesAndNonCharacters(line);
            StringBuilder key = new StringBuilder(currentWord.length());
            for(int i=0; i<currentWord.length(); i++) {
                char ch = currentWord.charAt(i);
                Integer integer = keypadMappings.get(ch);
                key.append(integer);
            }
            List<String> wordsList = dictionary.get(key.toString());
            if(wordsList == null) {
                wordsList = new ArrayList<String>();
            }
            // add current word to the bucket
            wordsList.add(currentWord);
            dictionary.put(key.toString(), wordsList);
        }
    }

    /**
     * Returns a list of possible matches for the provided key.
     *
     * @throws IllegalArgumentException when provided key is null
     *
     * @param key the key
     *
     * @return a list of matched words.
     */
    @Override
    public List<String> findMatch(String key) {
        ValidationUtils.assertNotNull(key);

        return dictionary.get(key);
    }
}
