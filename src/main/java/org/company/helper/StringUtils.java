package org.company.helper;

/**
 * Various string helper methods.
 *
 * @author Slava Ustovytski
 */
public final class StringUtils {

    public static final boolean isEmpty(String input) {
        return (input == null || input.isEmpty());
    }

    public static final boolean isNotEmpty(String input) {
        return !isEmpty(input);
    }

    public static final String stripWhitespacesAndNonDigits(String input) {
        String output = input.trim().toLowerCase();

        // Remove all the whitespaces
        output = output.replaceAll("\\s+", "");

        // Remove all the punctuation marks
        output = output.replaceAll("\\D+", "");

        return output;
    }

    public static final String stripWhitespacesAndNonCharacters(String input) {
        String output = input.trim().toLowerCase();

        // Remove all the whitespaces
        output = output.replaceAll("\\s+", "");

        // Remove all the punctuation marks
        output = output.replaceAll("\\W+", "");

        return output;
    }
}
