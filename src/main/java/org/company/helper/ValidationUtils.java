package org.company.helper;

/**
 * Validation helper class.
 *
 * @author Slava Ustovytski
 */
public final class ValidationUtils {

    /**
     * Will throw an IllegalArgumentException if provided argument is null.
     *
     * @param param
     */
    public static final void assertNotNull(Object param) {
        if(param == null) {
            throw new IllegalArgumentException();
        }
    }

}
