package org.company.parser;

import org.company.dictionary.PhoneNumberDictionary;
import org.company.helper.StringUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Default implementation of the PhoneNumberParser.
 *
 * @author Slava Ustovytski
 */
public class PhoneNumberParserImpl implements PhoneNumberParser {

    private PhoneNumberDictionary dictionary;

    public PhoneNumberParserImpl(PhoneNumberDictionary dictionary) {
        this.dictionary = dictionary;
    }

    /**
     * Produces a Map of all the possible word replacements (value) for each provided phone number (key).
     *
     * @param phoneNumbers List of provided phone numbers.
     *
     * @return A map contains a word replacements for the original phone numbers.
     */
    @Override
    public Map<String, List<ParsedPhoneNumber>> parse(List<String> phoneNumbers) {
        Map<String, List<ParsedPhoneNumber>> parsedData = new HashMap<String, List<ParsedPhoneNumber>>();

        for (String phoneNumber : phoneNumbers) {
            try {
                // remove all the white spaces and non digits from the input number.
                String strippedPhoneNumber = StringUtils.stripWhitespacesAndNonDigits(phoneNumber);
                if(StringUtils.isNotEmpty(strippedPhoneNumber)) {
                    List<ParsedPhoneNumber> parsedPhoneNumber = parsePhoneNumber(strippedPhoneNumber);
                        parsedData.put(strippedPhoneNumber, parsedPhoneNumber);
                }
            } catch (Throwable t) {
                // if error occurred while processing then skip the current entry and continue processing rest of items.
                System.out.println("ERROR: could not parse provided phone number - " + phoneNumber + ". Skipping the number.");
            }
        }

        return parsedData;
    }

    public List<ParsedPhoneNumber> parsePhoneNumber(String inputPhoneNumber) {
        List<ParsedPhoneNumber> parsedPhoneNumberParts = new ArrayList<ParsedPhoneNumber>();

        for(int i=0; i<inputPhoneNumber.length(); i++) {
            String skippedPart = "";

            // Could not match starting from first digit? move over to the second one keeping the first digit.
            if(i == 1) {
                skippedPart = String.valueOf(inputPhoneNumber.charAt(i - 1));
            }

            // Second unmatched digit? aborting...
            if(i == 2) {
                break;
            }

            for(int j=i+1; j<=inputPhoneNumber.length(); j++) {
                String prefix = inputPhoneNumber.substring(i, j);
                List<String> dictionaryMatch = dictionary.findMatch(prefix);

                if(dictionaryMatch != null && !dictionaryMatch.isEmpty()) {
                    // found a match in the dictionary
                    if(inputPhoneNumber.length() == j) {
                        // it's a last number. no need to call the function again recursively.
                        for(String matchedWord : dictionaryMatch) {
                            ParsedPhoneNumber matchedPart = new ParsedPhoneNumber(skippedPart);
                            matchedPart.addPart(matchedWord);
                            parsedPhoneNumberParts.add(matchedPart);
                        }
                    } else {
                        // not the last digit? continue looking for another matches...
                        // now we need to search recursively for matches starting from the next digit.
                        List<ParsedPhoneNumber> nextMatches = parsePhoneNumber(inputPhoneNumber.substring(j));
                        for(String matchedWord : dictionaryMatch) {
                            ParsedPhoneNumber matchedPart = new ParsedPhoneNumber(skippedPart);
                            matchedPart.addPart(matchedWord);
                            for(ParsedPhoneNumber nextMatch : nextMatches) {
                                ParsedPhoneNumber compoundMatch = new ParsedPhoneNumber(matchedPart);
                                compoundMatch.addAllParts(nextMatch);
                                parsedPhoneNumberParts.add(compoundMatch);
                            }
                        }
                    }
                } else if(inputPhoneNumber.length() == 1) {
                    // could not find a match and input phone number contains only one digit. just add it as a match.
                    ParsedPhoneNumber lastDigitUnchanged = new ParsedPhoneNumber(prefix);
                    parsedPhoneNumberParts.add(lastDigitUnchanged);
                }
            }
        }
        return  parsedPhoneNumberParts;
    }
}
