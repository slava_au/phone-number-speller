package org.company.parser;

import java.util.List;
import java.util.Map;

/**
 * Interface provides a common functionality to parse numeric phone number into a list of possible word numbers.
 *
 * For instance:
 * 225563 > [call,me], [ball,me], [2,all,me].
 *
 * This class will use a provided dictionary to search for a matches.
 *
 * @author Slava Ustovytski
 *
 */
public interface PhoneNumberParser {

    /**
     * Parses a list of provided numeric phone numbers to produce a list of possible letter replacements for every input number.
     *
     * @param phoneNumbers a list of phoneNumbers to parse.
     *
     * @return A map of all possible word replacements for each phone number.
     */
    Map<String, List<ParsedPhoneNumber>> parse(List<String> phoneNumbers);

}
