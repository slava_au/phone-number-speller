package org.company.parser;

/**
 * Command line default input parser implementation.
 *
 * @author Slava Ustovytski
 */
public class CommandLineParser {

    /**
     * Parses arguments array to produce a list of options and params.
     *
     * The format of the command line argument supported by this implementation is:
     * $ command_name [-d /path/to/dictionary/file] [/path/to/input/file] [/path/to/input/file1] ...
     *
     * @param args arrays of the command line arguments
     * @param commandLineOptions specified options to look for.
     *
     * @return an object containing a list of options and parameters.
     */
    public CommandLineOptions parse(String[] args, CommandLineOptions commandLineOptions) {
        for (int i = 0; i < args.length; i++) {
            String currentToken = args[i];
            if (currentToken.startsWith("-")) {
                // found an option
                CommandLineOptions.Option option = commandLineOptions.getOptionByName(currentToken);
                if (option != null && args.length > i + 1) {
                    // Option value must be a next token after it's name in the command line
                    String nextArgument = args[++i];
                    option.setValue(nextArgument);
                } else {
                    // we found an option name but no value.
                    System.err.printf("ERROR: Unknown command line option [%s] or not properly formatted.", currentToken);
                }
            } else {
                // if it's not an option then it's a param
                commandLineOptions.addParam(currentToken);
            }
        }

        return commandLineOptions;
    }
}
