package org.company.parser;

import org.company.formatter.PhoneNumberFormatter;
import org.company.helper.StringUtils;

import java.util.ArrayList;
import java.util.List;

/**
 * Parsed phone number parts holder.
 *
 * @author Slava Ustovytski
 */
public class ParsedPhoneNumber {

    private final List<String> parsedParts = new ArrayList<String>();

    public ParsedPhoneNumber() {
    }

    public ParsedPhoneNumber(String... parts) {
        for(String part : parts) {
            if (StringUtils.isNotEmpty(part)) {
                parsedParts.add(part);
            }
        }
    }

    public ParsedPhoneNumber(ParsedPhoneNumber... others) {
        for(ParsedPhoneNumber other : others) {
            if(other != null) {
                addAllParts(other);
            }
        }
    }

    public ParsedPhoneNumber addPart(String part) {
        parsedParts.add(part);
        return this;
    }

    public ParsedPhoneNumber addAllParts(ParsedPhoneNumber phoneNumber) {
        parsedParts.addAll(phoneNumber.parsedParts);
        return this;

    }

    public String getFormattedNumber(PhoneNumberFormatter formatter) {
        if(formatter == null) {
            return parsedParts.toString();
        }

        return formatter.format(parsedParts);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        ParsedPhoneNumber that = (ParsedPhoneNumber) o;

        if (parsedParts != null ? !parsedParts.equals(that.parsedParts) : that.parsedParts != null) return false;

        return true;
    }

    @Override
    public int hashCode() {
        return parsedParts != null ? parsedParts.hashCode() : 0;
    }
}
