package org.company.parser;

import org.company.helper.StringUtils;
import org.company.helper.ValidationUtils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Class which contains a list of options and parameters.
 *
 * @author Slava Ustovytski
 */
public class CommandLineOptions {

    private Map<String, Option> options = new HashMap<String, Option>();

    private List<String> params = new ArrayList<String>();

    public void addOption(String name, boolean isRequired) {
        options.put(name, new Option(isRequired));
    }

    public void addParam(String param) {
        ValidationUtils.assertNotNull(param);

        params.add(param);
    }

    public Option getOptionByName(String name) {
        ValidationUtils.assertNotNull(name);

        return options.get(name);
    }

    public List<String> getParams() {
        return params;
    }

    public class Option {

        private String value;
        private final boolean isRequired;

        public Option(boolean required) {
            this.isRequired = required;
        }

        public void setValue(String value) {
            this.value = value;
        }

        public String getValue() {
            return value;
        }

        public boolean isValid() {
            return !isRequired || StringUtils.isNotEmpty(value);
        }
    }
}
