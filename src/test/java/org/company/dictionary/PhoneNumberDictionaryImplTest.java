package org.company.dictionary;

import org.company.reader.InputReader;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItems;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhoneNumberDictionaryImplTest {

    private PhoneNumberDictionary dictionary;
    private InputReader dictionaryFileInputReader = mock(InputReader.class);

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Test
    public void findMatch_WillReturnMatchedWord_WhenMappedNumberExists() {
        // prepare
        initializeDictionary(Arrays.asList(" c ! a ' l ; l . ", "ball", "m^e"));

        // execute
        List<String> foundWord = dictionary.findMatch("2255");

        // assert
        assertThat(foundWord, hasItems("call", "ball"));
    }

    @Test(expected = IllegalArgumentException.class)
    public void findMatch_WillThrowAnException_WhenInputIsNull() {
        // prepare
        initializeDictionary(Arrays.asList("call", "me"));
        // execute
        dictionary.findMatch(null);
    }

    @Test
    public void init_WillExitProgram_WhenProvidedDictionaryFileIsEmptyOrNotFound() {
        // prepare
        when(dictionaryFileInputReader.readLines()).thenReturn(Collections.EMPTY_LIST);
        // execute
        // constructor triggers init() method
        exit.expectSystemExitWithStatus(1);
        dictionary = new PhoneNumberDictionaryImpl(dictionaryFileInputReader);
    }

    private void initializeDictionary(List<String> dictionaryContent) {
        when(dictionaryFileInputReader.readLines()).thenReturn(dictionaryContent);
        dictionary = new PhoneNumberDictionaryImpl(dictionaryFileInputReader);
    }
}
