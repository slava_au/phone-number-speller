package org.company.reader;

import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.contrib.java.lang.system.ExpectedSystemExit;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.List;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasItem;
import static org.hamcrest.Matchers.hasSize;
import static org.mockito.Mockito.*;

public class ConsoleInputReaderTest {

    private ConsoleInputReader reader;
    private BufferedReader mockConsole;

    @Rule
    public final ExpectedSystemExit exit = ExpectedSystemExit.none();

    @Before
    public void setup() {
        mockConsole = mock(BufferedReader.class);
        reader = new ConsoleInputReader(mockConsole);
    }

    @Test
    public void readLines_WillReadUserInputFRomConsole() throws IOException {
        // prepare
        ConsoleInputReader consoleReader = new ConsoleInputReader();
        when(mockConsole.readLine()).thenReturn("225563").thenReturn("");

        // execute
        List<String> readLines = reader.readLines();

        // assert
        assertThat(readLines, hasSize(1));
        assertThat(readLines, hasItem("225563"));
    }

    @Test
    public void readLine_WillExit_WhenErrorOccurredDuringReadingFromConsole() throws IOException {
        // prepare
        doThrow(new IOException()).when(mockConsole).readLine();
        doThrow(new IOException()).when(mockConsole).close();

        // execute
        exit.expectSystemExitWithStatus(1);
        List<String> readLines = reader.readLines();

        // assert
        assertThat(readLines, hasSize(0));
    }
}
