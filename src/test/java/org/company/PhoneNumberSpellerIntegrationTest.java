package org.company;

import org.company.dictionary.PhoneNumberDictionary;
import org.company.dictionary.PhoneNumberDictionaryImpl;
import org.company.formatter.PhoneNumberFormatter;
import org.company.formatter.PhoneNumberFormatterImpl;
import org.company.parser.PhoneNumberParser;
import org.company.parser.PhoneNumberParserImpl;
import org.company.reader.FileInputReader;
import org.company.reader.InputReader;
import org.hamcrest.Matchers;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.CoreMatchers.hasItems;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.hasKey;
import static org.hamcrest.Matchers.hasSize;

public class PhoneNumberSpellerIntegrationTest {

    private PhoneNumberSpeller speller;
    private InputReader inputReader;
    private PhoneNumberParser phoneNumberParser;
    private PhoneNumberFormatter outputFormatter;


    @Before
    public void setup() {
        inputReader = new FileInputReader(Arrays.asList("./input/sample-input.txt"));

        InputReader dictionaryFileInputReader = new FileInputReader(Arrays.asList("./input/sample-dictionary.txt"));
        PhoneNumberDictionary dictionary = new PhoneNumberDictionaryImpl(dictionaryFileInputReader);
        phoneNumberParser = new PhoneNumberParserImpl(dictionary);
        outputFormatter = new PhoneNumberFormatterImpl();

        speller =  new PhoneNumberSpeller();
        speller.setPhoneNumbersInputReader(inputReader);
        speller.setPhoneNumberParser(phoneNumberParser);
        speller.setOutputFormatter(outputFormatter);
    }

    @Test
    public void spell_WillReturnPhoneParts()  {
        // prepare
        String inputNumberFromFile1 = "225563";
        String inputNumberFromFile2 = "1632633";

        // execute
        Map<String, List<String>> userFriendlyPhoneNumber = speller.spell();

        // assert

        assertThat(userFriendlyPhoneNumber.size(), is(2));
        assertThat(userFriendlyPhoneNumber, hasKey(inputNumberFromFile1));
        List<String> matchePhoneNumbers1 = userFriendlyPhoneNumber.get(inputNumberFromFile1);
        assertThat(matchePhoneNumbers1, hasSize(3));
        assertThat(matchePhoneNumbers1, Matchers.hasItems("BALL-ME", "CALL-ME", "2-ALL-ME"));

        assertThat(userFriendlyPhoneNumber, hasKey(inputNumberFromFile2));
        List<String> matchePhoneNumbers2 = userFriendlyPhoneNumber.get(inputNumberFromFile2);
        assertThat(matchePhoneNumbers2, hasSize(8));
        assertThat(matchePhoneNumbers2, hasItems("1-ME-AND-3", "1-ME-BODE", "1-ME-CODE", "1-ME-2-ME-3", "1-ME-2-NEE", "1-ME-2-ODD",
                "1-ME-2-ODE", "1-ME-2-OFF"));
    }

    @Test
    public void main_WillNotThrowAnyExceptions_WhenProvidedArgumentsAreAsExpected() {
        // prepare

        // execute
        PhoneNumberSpeller.main(new String[] {"-d", "./input/minimal-dictionary.txt", "./input/sample-input.txt"});

        // assert
    }
}
