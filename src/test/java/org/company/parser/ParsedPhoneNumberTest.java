package org.company.parser;

import org.company.formatter.PhoneNumberFormatter;
import org.junit.Test;
import org.mockito.Mockito;

import java.util.Arrays;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.is;
import static org.mockito.Mockito.when;

public class ParsedPhoneNumberTest {

    @Test
    public void getFormattedNumber_WillCallFormatter_WhenItProvidedAsParameter() throws Exception {
        // prepare
        ParsedPhoneNumber parsedObject = new ParsedPhoneNumber("call", "me");
        PhoneNumberFormatter formatter = Mockito.mock(PhoneNumberFormatter.class);
        String expectedOutput = "CALL-ME";
        when(formatter.format(Arrays.asList("call", "me"))).thenReturn(expectedOutput);

        // execute
        String formattedNumber = parsedObject.getFormattedNumber(formatter);

        // assert
        assertThat(formattedNumber, equalTo(expectedOutput));
    }

    @Test
    public void getFormattedNumber_WillBasicString_WhenNoFormatterProvided() throws Exception {
        // prepare
        ParsedPhoneNumber parsedObject = new ParsedPhoneNumber("call", "me");
        String expectedOutput = "[call, me]";

        // execute
        String formattedNumber = parsedObject.getFormattedNumber(null);

        // assert
        assertThat(formattedNumber, equalTo(expectedOutput));
    }

    @Test
    public void addAllParts_WillPopulateAllPartsFromProvidedObject() {
        // prepare
        ParsedPhoneNumber sourceObject = new ParsedPhoneNumber("call", "me");
        ParsedPhoneNumber targetObject = new ParsedPhoneNumber(sourceObject);

        // execute
        boolean equals = targetObject.equals(sourceObject);

        // assert
        assertThat(equals, is(true));

    }

    @Test
    public void equals_WillReturnTrue_WhenObjectsAreTheSame() {
        // prepare
        ParsedPhoneNumber parsedObject = new ParsedPhoneNumber();
        parsedObject.addPart("call");

        // execute
        boolean equals = parsedObject.equals(new ParsedPhoneNumber("call"));

        // assert
        assertThat(equals, is(true));
    }

    @Test
    public void hashCode_WillBeTheSame_WhenObjectsAreTheSame() {
        // prepare
        ParsedPhoneNumber parsedObject = new ParsedPhoneNumber();
        parsedObject.addPart("call");

        // execute
        int hc1 = parsedObject.hashCode();
        int hc2 = new ParsedPhoneNumber("call").hashCode();

        // assert
        assertThat(hc1, equalTo(hc2));
    }

}
