package org.company.parser;

import org.company.dictionary.PhoneNumberDictionary;
import org.company.dictionary.PhoneNumberDictionaryImpl;
import org.junit.Before;
import org.junit.Test;

import java.util.Arrays;
import java.util.List;
import java.util.Map;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;
import static org.mockito.Mockito.doThrow;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class PhoneNumberParserImplTest {

    private PhoneNumberParserImpl parser;
    private PhoneNumberDictionary dictionary;

    @Before
    public void setUp() throws Exception {
        dictionary = mock(PhoneNumberDictionaryImpl.class);
        parser = new PhoneNumberParserImpl(dictionary);
    }

    @Test
    public void parse_WillParseProvidedPhoneNumberRecursively_WhenDictionaryContainsRequiredWords() {
        // prepare
        when(dictionary.findMatch("2255")).thenReturn(Arrays.asList("ball", "call"));
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));
        when(dictionary.findMatch("255")).thenReturn(Arrays.asList("all"));
        String inputNumber = "225563";
        List<String> inputPhoneNumbers = Arrays.asList(inputNumber);

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(1));
        assertThat(parsedPhoneNumbers, hasKey(inputNumber));

        List<ParsedPhoneNumber> matchedPhoneNumbers = parsedPhoneNumbers.get(inputNumber);
        assertThat(matchedPhoneNumbers, hasSize(3));
        assertThat(matchedPhoneNumbers, hasItems(new ParsedPhoneNumber("call", "me"),
                new ParsedPhoneNumber("ball", "me"), new ParsedPhoneNumber("2", "all", "me")));
    }

    @Test
    public void parse_WillParseProvidedListOfPhoneNumbersRecursively_WhenListOfNumbersFedAsAnInput() {
        // prepare
        String inputNumber1 = "225563";
        String inputNumber2 = "1632633";
        List<String> inputPhoneNumbers = Arrays.asList(inputNumber1, inputNumber2);
        when(dictionary.findMatch("2255")).thenReturn(Arrays.asList("ball", "call"));
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));
        when(dictionary.findMatch("255")).thenReturn(Arrays.asList("all"));

        when(dictionary.findMatch("263")).thenReturn(Arrays.asList("and"));
        when(dictionary.findMatch("2633")).thenReturn(Arrays.asList("bode", "code"));
        when(dictionary.findMatch("633")).thenReturn(Arrays.asList("odd", "off"));


        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(2));
        assertThat(parsedPhoneNumbers, hasKey(inputNumber1));
        List<ParsedPhoneNumber> matchedPhoneNumbers1 = parsedPhoneNumbers.get(inputNumber1);
        assertThat(matchedPhoneNumbers1, hasSize(3));
        assertThat(matchedPhoneNumbers1, hasItems(new ParsedPhoneNumber("call", "me"),
                new ParsedPhoneNumber("ball", "me"), new ParsedPhoneNumber("2", "all", "me")));

        assertThat(parsedPhoneNumbers, hasKey(inputNumber2));
        List<ParsedPhoneNumber> matchedPhoneNumbers2 = parsedPhoneNumbers.get(inputNumber2);
        assertThat(matchedPhoneNumbers2, hasSize(6));
        assertThat(matchedPhoneNumbers2, hasItems(new ParsedPhoneNumber("1", "me", "and", "3"),
                new ParsedPhoneNumber("1", "me", "bode"),
                new ParsedPhoneNumber("1", "me", "code"),
                new ParsedPhoneNumber("1", "me", "2", "me", "3"),
                new ParsedPhoneNumber("1", "me", "2", "odd"),
                new ParsedPhoneNumber("1", "me", "2", "off")));
    }

    @Test
    public void parse_WillLeaveSingeDigit_WhenNoMatchIsFoundInDictionary() {
        // prepare
        String inputNumber = "1632633";
        List<String> inputPhoneNumbers = Arrays.asList(inputNumber);
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(1));
        List<ParsedPhoneNumber> matchedPhoneNumbers = parsedPhoneNumbers.get(inputNumber);
        assertThat(matchedPhoneNumbers, hasSize(1));
        ParsedPhoneNumber expectedItem = new ParsedPhoneNumber("1", "me", "2", "me", "3");
        assertThat(matchedPhoneNumbers, hasItem(expectedItem));
    }

    @Test
    public void parse_WillLeaveOutWholeNumber_WhenTwoConsecutiveDigitsAreNotParsed() {
        // prepare
        String inputPhoneNumber = "1632734";
        List<String> inputPhoneNumbers = Arrays.asList(inputPhoneNumber);
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(1));
        assertThat(parsedPhoneNumbers, hasKey(inputPhoneNumber));
        List<ParsedPhoneNumber> matchedPhoneNumbers = parsedPhoneNumbers.get(inputPhoneNumber);
        assertThat(matchedPhoneNumbers, hasSize(0));
    }

    @Test
    public void parse_WillReturnNothing_WhenEmptyNumberIsProvided() {
        // prepare
        List<String> inputPhoneNumbers = Arrays.asList("");

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(0));
    }

    @Test
    public void parse_WillStripAllTheWhitespacesAndPunctuationMarks() {
        // prepare
        List<String> inputPhoneNumbers = Arrays.asList("2. 2. 5. 5,6 ! 3", "1 ,6 .3 )2 ?6 (3 ^3");
        when(dictionary.findMatch("2255")).thenReturn(Arrays.asList("call"));
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(2));
        assertThat(parsedPhoneNumbers, hasKey("225563"));
        List<ParsedPhoneNumber> matchedPhoneNumbers1 = parsedPhoneNumbers.get("225563");
        assertThat(matchedPhoneNumbers1, hasSize(1));
        assertThat(matchedPhoneNumbers1, hasItems(new ParsedPhoneNumber("call", "me")));

        assertThat(parsedPhoneNumbers, hasKey("1632633"));
        List<ParsedPhoneNumber> matchedPhoneNumbers2 = parsedPhoneNumbers.get("1632633");
        assertThat(matchedPhoneNumbers2, hasItems(new ParsedPhoneNumber("1", "me", "2", "me", "3")));
    }

    @Test
    public void parse_WillContinueParsingOtherNumbers_WhenParsingOneOfThemCausedError() {
        // prepare
        List<String> inputPhoneNumbers = Arrays.asList("225563", "1632633");
        doThrow(new RuntimeException()).when(dictionary).findMatch("2255");
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(1));
        assertThat(parsedPhoneNumbers, hasKey("1632633"));
        List<ParsedPhoneNumber> matchedPhoneNumbers = parsedPhoneNumbers.get("1632633");
        assertThat(matchedPhoneNumbers, hasItems(new ParsedPhoneNumber("1", "me", "2", "me", "3")));
    }

    @Test
    public void parse_WillSkipTheProvidedNumber_WhenNumberContainsChars() {
        // prepare
        List<String> inputPhoneNumbers = Arrays.asList("1-me-63-2-me-63-3-me");
        when(dictionary.findMatch("63")).thenReturn(Arrays.asList("me"));

        // execute
        Map<String, List<ParsedPhoneNumber>> parsedPhoneNumbers = parser.parse(inputPhoneNumbers);

        // assert
        assertThat(parsedPhoneNumbers.size(), is(1));
        assertThat(parsedPhoneNumbers, hasKey("1632633"));
        List<ParsedPhoneNumber> matchedPhoneNumbers = parsedPhoneNumbers.get("1632633");
        assertThat(matchedPhoneNumbers, hasItem(new ParsedPhoneNumber("1", "me", "2", "me", "3")));
    }
}
