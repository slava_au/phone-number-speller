package org.company.parser;

import org.junit.Test;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.*;

public class CommandLineParserTest {

    private CommandLineParser parser = new CommandLineParser();

    @Test
    public void parse_WillFindOptionValueAndParams_WhenProvidedArgumentsContainsThem() {
        // prepare
        String dictOptionName = "-d";
        String[] args = {dictOptionName, "path/to/dictionary/file", "path/to/input/file/1", "path/to/input/file/2"};
        CommandLineOptions commandLineOptions = new CommandLineOptions();
        commandLineOptions.addOption(dictOptionName, true);

        // execute
        parser.parse(args, commandLineOptions);

        // assert
        assertThat(commandLineOptions.getOptionByName(dictOptionName).isValid(), is(true));
        assertThat(commandLineOptions.getOptionByName(dictOptionName).getValue(), is("path/to/dictionary/file"));
        assertThat(commandLineOptions.getParams(), hasSize(2));
        assertThat(commandLineOptions.getParams(), hasItems("path/to/input/file/1", "path/to/input/file/2"));
    }

    @Test
    public void parse_WillSkipOption_WhenNoOptionProvidedInArguments() {
        // prepare
        String[] args = {"path/to/input/file/1", "path/to/input/file/2"};
        String optionName = "-d";
        CommandLineOptions commandLineOptions = new CommandLineOptions();
        commandLineOptions.addOption(optionName, true);

        // execute
        parser.parse(args, commandLineOptions);

        // assert
        assertThat(commandLineOptions.getOptionByName(optionName).isValid(), is(false));
        assertThat(commandLineOptions.getOptionByName(optionName).getValue(), nullValue());
        assertThat(commandLineOptions.getParams(), hasSize(2));
        assertThat(commandLineOptions.getParams(), hasItems("path/to/input/file/1", "path/to/input/file/2"));
    }

    @Test
    public void parse_WillSkipOption_WhenOptionValueIsMissed() {
        // prepare
        String[] args = {"-d"};
        CommandLineOptions commandLineOptions = new CommandLineOptions();
        commandLineOptions.addOption("-d", false);

        // execute
        parser.parse(args, commandLineOptions);

        // assert
        assertThat(commandLineOptions.getOptionByName("-d").getValue(), nullValue());
        assertThat(commandLineOptions.getParams(), hasSize(0));
    }
}
