package org.company.formatter;

import org.junit.Test;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.CoreMatchers.equalTo;
import static org.hamcrest.MatcherAssert.assertThat;

public class PhoneNumberFormatterImplTest {

    private PhoneNumberFormatter formatter = new PhoneNumberFormatterImpl();

    @Test(expected = IllegalArgumentException.class)
    public void format_WillThrowAnException_WhenArgumentPassedIsNull() {
        // execute
        formatter.format(null);
    }

    @Test
    public void format_WillFormatProvidedNumberParts() throws Exception {
        // prepare
        List<String> parts = Arrays.asList("call", "1", "me");

        // execute
        String formattedNumber = formatter.format(parts);

        // assert
        assertThat(formattedNumber, equalTo("CALL-1-ME"));
    }


}
