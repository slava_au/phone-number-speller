package org.company.helper;

import org.junit.Test;

public class ValidationUtilsTest {

    @Test(expected = IllegalArgumentException.class)
    public void assertNotNull_WillThrowException_WhenInputParamIsNull() throws Exception {
        // execute
        ValidationUtils.assertNotNull(null);
    }
}
